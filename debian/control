Source: libcode-tidyall-perl
Section: perl
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 git <!nocheck>,
 iamerican <!nocheck>,
 ispell <!nocheck>,
 libcapture-tiny-perl <!nocheck>,
 libcode-tidyall-plugin-sortlines-naturally-perl <!nocheck>,
 libconfig-ini-perl <!nocheck>,
 libfile-pushd-perl <!nocheck>,
 libfile-which-perl <!nocheck>,
 libfile-zglob-perl <!nocheck>,
 libipc-run3-perl <!nocheck>,
 libipc-system-simple-perl <!nocheck>,
 liblib-relative-perl <!nocheck>,
 liblist-compare-perl <!nocheck>,
 liblist-someutils-perl <!nocheck>,
 liblog-any-perl <!nocheck>,
 libmoo-perl <!nocheck>,
 libparallel-forkmanager-perl <!nocheck>,
 libpath-tiny-perl <!nocheck>,
 libperl-critic-perl <!nocheck>,
 libpod-spell-perl <!nocheck>,
 libscope-guard-perl <!nocheck>,
 libspecio-library-path-tiny-perl <!nocheck>,
 libspecio-perl <!nocheck>,
 libtest-class-most-perl <!nocheck>,
 libtest-differences-perl <!nocheck>,
 libtest-fatal-perl <!nocheck>,
 libtest-warnings-perl <!nocheck>,
 libtext-diff-perl <!nocheck>,
 libtime-duration-parse-perl <!nocheck>,
 libtimedate-perl <!nocheck>,
 libtry-tiny-perl <!nocheck>,
 nodejs <!nocheck>,
 perl,
 perltidier <!nocheck>,
 perltidy <!nocheck>,
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders:
 Jonas Smedegaard <dr@jones.dk>,
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libcode-tidyall-perl.git
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libcode-tidyall-perl
Homepage: https://metacpan.org/release/Code-TidyAll
Testsuite: autopkgtest-pkg-perl
Rules-Requires-Root: no

Package: libcode-tidyall-perl
Architecture: all
Depends:
 libcapture-tiny-perl,
 libconfig-ini-perl,
 libfile-pushd-perl,
 libfile-which-perl,
 libfile-zglob-perl,
 libipc-run3-perl,
 libipc-system-simple-perl,
 liblist-compare-perl,
 liblist-someutils-perl,
 liblog-any-perl,
 libmoo-perl,
 libpath-tiny-perl,
 libscope-guard-perl,
 libspecio-library-path-tiny-perl,
 libspecio-perl,
 libtext-diff-perl,
 libtime-duration-parse-perl,
 libtimedate-perl,
 libtry-tiny-perl,
 ${misc:Depends},
 ${perl:Depends}
Recommends:
 ispell,
 libjson-maybexs-perl,
 libparallel-forkmanager-perl,
 libperl-critic-perl,
 libpod-spell-perl,
 libsvn-look-perl,
 perltidier,
 perltidy,
Suggests:
 php-codesniffer,
Enhances:
 git,
 subversion,
Description: your all-in-one code tidier and validator
 There are a lot of great code tidiers and validators out there.
 tidyall makes them available from a single unified interface.
 .
 You can run tidyall on a single file or on an entire project hierarchy,
 and configure which tidiers/validators are applied to which files.
 tidyall will back up files beforehand,
 and for efficiency will only consider files that have changed
 since they were last processed.
 .
 This package provides commandline tool tidyall
 and Perl module Code::TidyAll.
 .
 Includes hooks for Git and Subversion,
 and plugin for php-codesniffer,
 requiring corresponding packages installed.
